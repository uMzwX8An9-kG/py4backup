#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#Requres python3
__author__ = 'larrabee'

import argparse

version = '0.1.03'

parser = argparse.ArgumentParser()
parser.add_argument('-file', '--first', help='Full backup')
parser.add_argument('-s', '--second', help='If backup: current state; If restore: diff file')
parser.add_argument('-r', '--result', help=' Output file. If backup: diff file; If restore: diff file')
parser.add_argument('-v', '--version', default=False, action='store_true', help='Display program version')
parser.add_argument('--bs', default=4096, help='Block size')
parser.add_argument('--restore', default=False, action='store_true', help='Restore mod. Create diff from diff')
args = parser.parse_args()


class ReadFile():
    def __init__(self, filename, diff=False, chunksize=4096, counter_len=32):
        self.counter_len = counter_len
        self.chunksize = chunksize
        self.diff = diff
        self.filename = filename
        if diff:
            self.data = self.read_diff()
        else:
            self.data = self.read_full()
    i = 0

    def next(self):
        if self.diff:
            try:
                element = next(self.data)
            except StopIteration:
                element_data = None
                element_counter = None
            else:
                element_counter = int(element[:32], 2)
                element_data = element[32:]
            return element_counter, element_data
        else:
            try:
                element = next(self.data)
            except StopIteration:
                element = None
            return element

    def read_full(self):
        with open(self.filename, "rb") as f:
            while True:
                chunk = f.read(self.chunksize)
                if chunk:
                    yield chunk
                else:
                    break

    def read_diff(self):
        with open(self.filename, "rb") as f:
            while True:
                chunk = f.read(self.chunksize + self.counter_len)
                if chunk:
                    yield chunk
                else:
                    break


class WriteFile():
    def __init__(self, filename, diff=False, counter_len=32):
        self.counter_len = counter_len
        self.diff = diff
        self.filename = filename

    def write_full(self, data):
        with open(self.filename, "ab") as f:
            f.write(data)

    def write_diff(self, data, number):
        number = bytes(str(bin(number)[2:]).rjust(self.counter_len, '0').encode(encoding='ASCII'))
        with open(self.filename, "ab") as f:
            f.write(number + data)


def create_diff(full_backup, current_backup, result):
    read_full = ReadFile(full_backup, chunksize=args.bs)
    read_diff = ReadFile(current_backup, chunksize=args.bs)
    write_result = WriteFile(result, diff=True)
    i = 1
    while True:
        full_data = read_full.next()
        diff_data = read_diff.next()
        if (full_data is None) or (diff_data is None):
            break
        if full_data != diff_data:
            write_result.write_diff(data=diff_data, number=i)
        i += 1


def create_full_from_diff(full_backup, diff_file, result):
    read_full = ReadFile(full_backup, chunksize=args.bs)
    read_diff = ReadFile(diff_file, diff=True, chunksize=args.bs)
    write_result = WriteFile(result)
    full_can_read = True
    i = 1
    while True:
        block_num, diff_data = read_diff.next()
        if full_can_read:
            while True:
                full_data = read_full.next()
                if block_num == i:
                    write_result.write_full(diff_data)
                    i += 1
                    break
                else:
                    if full_data is None:
                        full_can_read = False
                        break
                    write_result.write_full(full_data)
                    i += 1
        else:
            if block_num is None:
                break
            else:
                write_result.write_full(diff_data)


if args.version:
    print('Version: ', version, '\nLicense: GPLv3\nAuthor: larrabee@nixdi.com')
    exit(0)

if args.restore:
    create_full_from_diff(full_backup=args.first, diff_file=args.second, result=args.result)
else:
    create_diff(full_backup=args.first, current_backup=args.second, result=args.result)
