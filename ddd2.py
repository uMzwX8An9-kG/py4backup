#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#Requres python3
__author__ = 'larrabee'

import argparse
import binascii
import hashlib

version = '0.1.05'

parser = argparse.ArgumentParser()
parser.add_argument('-file', '--first', help='Full backup')
parser.add_argument('-s', '--second', help='If backup: current state; If restore: diff file')
parser.add_argument('-r', '--result', help=' Output file. If backup: diff file; If restore: diff file')
parser.add_argument('-v', '--version', default=False, action='store_true', help='Display program version')
parser.add_argument('--bs', default=4096, help='Block size')
parser.add_argument('--hash', default=None, help='Hash algorithm. Default is None. You can different between'
                                                 ' md5 and crc32')
parser.add_argument('--restore', default=False, action='store_true', help='Restore mod. Create diff from diff')
args = parser.parse_args()


class ReadFile():
    def __init__(self, filename, diff_map=False, chunksize=4096, counter_len=32):
        self.counter_len = counter_len
        self.chunksize = chunksize
        self.diff = diff_map
        self.filename = filename
        if diff_map:
            self.file = open(self.filename, "r")
            self.data = self.__read_diff()
        else:
            self.file = open(self.filename, "rb")
            self.data = self.__read_full()

    i = 0

    def next(self, without_formatting=False):
        if self.diff:
            try:
                element = next(self.data)
            except StopIteration:
                checksumm = None
                control = None
            else:
                control = str(element.split(sep=':')[0])
                checksumm = str(element.split(sep=':')[1])
            if without_formatting:
                return element
            else:
                return control, checksumm
        else:
            try:
                element = next(self.data)
            except StopIteration:
                element = None
            return element

    def __read_full(self):
        while True:
            chunk = self.file.read(self.chunksize)
            if chunk:
                yield chunk
            else:
                break

    def __read_diff(self):
        while True:
            chunk = self.file.readline()
            if chunk:
                yield chunk
            else:
                break

    def close(self):
        self.file.close()


class WriteFile():
    def __init__(self, filename, diff_map=False):
        self.filename = filename
        self.diff = diff_map
        if diff_map:
            self.file = open(self.filename, "a", encoding='ASCII')
        else:
            self.file = open(self.filename, "ab")

    def write(self, data=None, control='', checksumm='', custom=None):
        if self.diff:
            if custom is None:
                self.file.write(str(control) + ':' + str(checksumm) + '\n')
            else:
                self.file.write(str(custom) + '\n')
        else:
            self.file.write(data)

    def close(self):
        self.file.close()


def create_diff(full_backup, current_backup, result):
    read_full = ReadFile(full_backup, chunksize=args.bs)
    read_diff = ReadFile(current_backup, chunksize=args.bs)
    write_diff = WriteFile(result + '.dd')
    write_diff_map = WriteFile(result + '.ddm', diff_map=True)
    write_diff_map.write(
        custom=str(full_backup) + ':' + str(args.bs) + ':' + str(args.hash))
    total_blocks = 1
    changed_blocks = 0
    while True:
        full_data = read_full.next()
        diff_data = read_diff.next()
        if diff_data is None:
            write_diff_map.write(control=str(0))
            break
        if full_data != diff_data:
            if args.hash == 'md5':
                write_diff_map.write(control=str(1), checksumm=hashlib.md5(diff_data).hexdigest())
            elif args.hash == 'crc32':
                write_diff_map.write(control=str(1), checksumm=binascii.crc32(diff_data))
            else:
                write_diff_map.write(control=str(1))
            write_diff.write(diff_data)
            changed_blocks += 1
        else:
            if args.hash == 'md5':
                write_diff_map.write(control=str(2), checksumm=hashlib.md5(full_data).hexdigest())
            elif args.hash == 'crc32':
                write_diff_map.write(control=str(2), checksumm=binascii.crc32(full_data))
            else:
                write_diff_map.write(control=str(2))
        total_blocks += 1
    read_full.close()
    read_diff.close()
    write_diff.close()
    write_diff_map.close()
    print('Total read blocks: ', total_blocks)
    print('Changed blocks: ', changed_blocks)


def restore_auto(diff_file, result):
    read_diff_map = ReadFile(diff_file + '.ddm', diff_map=True)
    control_string = read_diff_map.next(without_formatting=True)
    #Read values from .ddm file
    full_backup = control_string.split(sep=':')[0]
    args.bs = int(control_string.split(sep=':')[1])
    args.hash = control_string.split(sep=':')[2]
    print('Full backup:', full_backup)
    print('bs:', args.bs, '\nHash algorithm:', args.hash, end='')
    read_diff = ReadFile(diff_file + '.dd', chunksize=args.bs)
    read_full = ReadFile(full_backup, chunksize=args.bs)
    write_result = WriteFile(result)
    corrupted_blocks = []
    i = 1
    while True:
        control_symbol, block_hash = read_diff_map.next()
        full_data = read_full.next()
        if control_symbol == '0':
            break
        elif control_symbol == '1':
            diff_data = read_diff.next()
            write_result.write(diff_data)
            if 'md5' in args.hash:
                if hashlib.md5(diff_data).hexdigest() not in block_hash:
                    corrupted_blocks.append('diff' + str(i))
            elif 'crc32' in args.hash:
                if str(binascii.crc32(diff_data)) not in str(block_hash):
                    corrupted_blocks.append('diff' + str(i))
        elif control_symbol == '2':
            write_result.write(full_data)
            if 'md5' in args.hash:
                if hashlib.md5(full_data).hexdigest() not in block_hash:
                    corrupted_blocks.append('full' + str(i))
            elif 'crc32' in args.hash:
                if str(binascii.crc32(full_data)) not in str(block_hash):
                    corrupted_blocks.append('full' + str(i))
        i += 1
    read_full.close()
    read_diff.close()
    write_result.close()
    read_diff_map.close()
    print('Corrupted blocks:', corrupted_blocks)


if args.version:
    print('Version: ', version, '\nLicense: GPLv3\nAuthor: larrabee@nixdi.com')
    exit(0)

if args.restore:
    restore_auto(diff_file=args.second, result=args.result)
else:
    create_diff(full_backup=args.first, current_backup=args.second, result=args.result)